﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MeFit.Migrations
{
    public partial class WithProgrammesAndGoals : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "WorkoutSetRelations");

            migrationBuilder.CreateTable(
                name: "JoinWorkoutExerciseSets",
                columns: table => new
                {
                    WorkoutId = table.Column<int>(nullable: false),
                    ExerciseSetId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JoinWorkoutExerciseSets", x => new { x.WorkoutId, x.ExerciseSetId });
                    table.ForeignKey(
                        name: "FK_JoinWorkoutExerciseSets_ExerciseSets_ExerciseSetId",
                        column: x => x.ExerciseSetId,
                        principalTable: "ExerciseSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JoinWorkoutExerciseSets_Workouts_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Programmes",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    ProfileId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Programmes", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Programmes_Profiles_ProfileId",
                        column: x => x.ProfileId,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Goals",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Achieved = table.Column<int>(nullable: false),
                    ProgrammeId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Goals", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Goals_Programmes_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "Programmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JoinProgrammeWorkouts",
                columns: table => new
                {
                    ProgrammeId = table.Column<int>(nullable: false),
                    WorkoutId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JoinProgrammeWorkouts", x => new { x.ProgrammeId, x.WorkoutId });
                    table.ForeignKey(
                        name: "FK_JoinProgrammeWorkouts_Programmes_ProgrammeId",
                        column: x => x.ProgrammeId,
                        principalTable: "Programmes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JoinProgrammeWorkouts_Workouts_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "JoinGoalWorkouts",
                columns: table => new
                {
                    GoalId = table.Column<int>(nullable: false),
                    WorkoutId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JoinGoalWorkouts", x => new { x.GoalId, x.WorkoutId });
                    table.ForeignKey(
                        name: "FK_JoinGoalWorkouts_Goals_GoalId",
                        column: x => x.GoalId,
                        principalTable: "Goals",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_JoinGoalWorkouts_Workouts_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Goals_ProgrammeId",
                table: "Goals",
                column: "ProgrammeId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinGoalWorkouts_WorkoutId",
                table: "JoinGoalWorkouts",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinProgrammeWorkouts_WorkoutId",
                table: "JoinProgrammeWorkouts",
                column: "WorkoutId");

            migrationBuilder.CreateIndex(
                name: "IX_JoinWorkoutExerciseSets_ExerciseSetId",
                table: "JoinWorkoutExerciseSets",
                column: "ExerciseSetId");

            migrationBuilder.CreateIndex(
                name: "IX_Programmes_ProfileId",
                table: "Programmes",
                column: "ProfileId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JoinGoalWorkouts");

            migrationBuilder.DropTable(
                name: "JoinProgrammeWorkouts");

            migrationBuilder.DropTable(
                name: "JoinWorkoutExerciseSets");

            migrationBuilder.DropTable(
                name: "Goals");

            migrationBuilder.DropTable(
                name: "Programmes");

            migrationBuilder.CreateTable(
                name: "WorkoutSetRelations",
                columns: table => new
                {
                    WorkoutId = table.Column<int>(type: "int", nullable: false),
                    ExerciseSetId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkoutSetRelations", x => new { x.WorkoutId, x.ExerciseSetId });
                    table.ForeignKey(
                        name: "FK_WorkoutSetRelations_ExerciseSets_ExerciseSetId",
                        column: x => x.ExerciseSetId,
                        principalTable: "ExerciseSets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkoutSetRelations_Workouts_WorkoutId",
                        column: x => x.WorkoutId,
                        principalTable: "Workouts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkoutSetRelations_ExerciseSetId",
                table: "WorkoutSetRelations",
                column: "ExerciseSetId");
        }
    }
}
