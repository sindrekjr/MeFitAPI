using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using MeFit.DAL;
using MeFit.Models;

namespace MeFit.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProfileController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly ILogger<ProfileController> _logger;

        public ProfileController(MeFitDbContext context, ILogger<ProfileController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Profile>>> Get() => await _context.Profiles.Include("Address").Include(p => p.Programmes).Include(p => p.Workouts).Include(p => p.ExerciseSets).ToListAsync();

        [HttpGet("{id}")]
        public async Task<ActionResult<Profile>> Get(int id) => await _context.Profiles.Where(p => p.Id == id).Include("Address").Include(p => p.Programmes).Include(p => p.Workouts).Include(p => p.ExerciseSets).SingleOrDefaultAsync();

        [HttpPost]
        public async Task<ActionResult<Profile>> Post([FromBody] Profile profile)
        {
            _context.Profiles.Add(profile);
            await _context.SaveChangesAsync();
            return CreatedAtAction("Get", new { id = profile.Id }, profile);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Profile>> Delete(int id) => await Delete(await _context.Profiles.SingleAsync(p => p.Id == id));

        [HttpDelete]
        public async Task<ActionResult<Profile>> Delete([FromBody] Profile profile)
        {
            profile = _context.Profiles.Remove(profile).Entity;
            await _context.SaveChangesAsync();
            return profile;
        }
    }
}
