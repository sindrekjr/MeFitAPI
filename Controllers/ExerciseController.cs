using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using MeFit.DAL;
using MeFit.Models;

namespace MeFit.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ExerciseController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly ILogger<ExerciseController> _logger;

        public ExerciseController(MeFitDbContext context, ILogger<ExerciseController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Exercise> Get() => _context.Exercises;

        [HttpGet("{id}")]
        public Exercise Get(int id) => _context.Exercises.Find(id);
    }
}