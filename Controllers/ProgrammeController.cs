using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using MeFit.DAL;
using MeFit.Models;

namespace MeFit.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProgrammeController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly ILogger<ProgrammeController> _logger;

        public ProgrammeController(MeFitDbContext context, ILogger<ProgrammeController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Programme>>> Get() => await _context.Programmes.Include(p => p.Workouts).Include(p => p.Goals).ToListAsync();

        [HttpGet("{id}")]
        public async Task<ActionResult<Programme>> Get(int id) => await _context.Programmes.Where(p => p.Id == id).Include(p => p.Workouts).Include(p => p.Goals).SingleOrDefaultAsync();

        [HttpPost]
        public async Task<ActionResult<Programme>> Post([FromBody] Programme programme)
        {
            _context.Programmes.Add(programme);
            await _context.SaveChangesAsync();
            return CreatedAtAction("Get", new { id = programme.Id }, programme);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Programme>> Delete(int id) => await Delete(await _context.Programmes.SingleAsync(p => p.Id == id));

        [HttpDelete]
        public async Task<ActionResult<Programme>> Delete([FromBody] Programme programme)
        {
            programme = _context.Programmes.Remove(programme).Entity;
            await _context.SaveChangesAsync();
            return programme;
        }
    }
}
