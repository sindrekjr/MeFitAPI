using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

using MeFit.DAL;
using MeFit.Models;

namespace MeFit.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly ILogger<ProfileController> _logger;

        public UserController(MeFitDbContext context, ILogger<ProfileController> logger)
        {
            _context = context;
            _logger = logger;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<User>>> Get() => await _context.Users.Include("Profile").ToListAsync();

        [HttpGet("{id}")]
        public async Task<ActionResult<User>> Get(int id) => await _context.Users.Where(u => u.Id == id).Include("Profile").SingleOrDefaultAsync();

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;
            _context.Entry(user.Profile).State = EntityState.Modified;
            _context.Entry(user.Profile.Address).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<User>> Post(User user)
        {
            _context.Users.Add(user);
            await _context.SaveChangesAsync();
            return CreatedAtAction("Get", new { id = user.Id }, user);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> Delete(int id) => await Delete(await _context.Users.SingleAsync(u => u.Id == id));

        public async Task<ActionResult<User>> Delete([FromBody] User user)
        {
            user = _context.Users.Remove(user).Entity;
            await _context.SaveChangesAsync();
            return user;
        }

        private bool UserExists(int id) => _context.Users.Any(e => e.Id == id);
    }
}
