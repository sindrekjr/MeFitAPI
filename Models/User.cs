﻿using System;

namespace MeFit.Models
{
    public class User
    {
        /** Id **/
        public int Id { get; set; }

        /**
         * Profile
         */
        public virtual Profile Profile { get; set; }

        /**
         * Personal Information
         */
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get => $"{FirstName} {LastName}".Trim(); }

        /**
         * Access & Authentication
         * !! TO-DO: Ignore sensitive fields somehow. [IgnoreDataMember]?
         */
        public string Username { get; set; }
        public string Password { get; set; }
        public virtual Role Role { get; set; } = Role.User;
    }

    [Flags]
    public enum Role
    {
        Create, Read, Update, Delete,

        User = (Read),
        Contributor = (Read | Update),
        Admin = (Create | Read | Update | Delete)
    }
}
