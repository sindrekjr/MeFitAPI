using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace MeFit.Models
{
    public class Goal
    {
        /** Id **/
        public int Id { get; set; }

        public int Achieved { get; set; }

        /**
         * End Date
         */
        private DateTime? _endDate;
        public virtual DateTime? EndDate
        {
            get;// => _endDate ??  Workouts.OrderBy(w => w.EndDate).Single().EndDate;
            set;// => _endDate = value;
        }

        /**
         * Programme
         */
        public int ProgrammeId { get; set; }
        [JsonIgnore]
        public virtual Programme Programme { get; set; }

        /**
         * Workouts
         */
        public virtual List<Join_GoalWorkout> Workouts { get; set; }
    }
}