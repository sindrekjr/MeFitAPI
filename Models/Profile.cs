﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MeFit.Models
{
    public class Profile
    {
        /** Id **/
        public int Id { get; set; }

        /**
         * Access User
         */
        public int UserId { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }

        /**
         * Personal Information
         */
        public int Height { get; set; }
        public int Weight { get; set; }
        public int AddressId { get; set; }
        public virtual Address Address { get; set; }
        public string Disabilities { get; set; }
        public string MedicalConditions { get; set; }

        /**
         * Workout-related objects used and managed by this profile
         */
        public virtual List<Programme> Programmes { get; set; }
        public virtual List<Workout> Workouts { get; set; }
        public virtual List<ExerciseSet> ExerciseSets { get; set; }
    }
}
