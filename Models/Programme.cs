using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MeFit.Models
{
    public class Programme
    {
        /** Id **/
        public int Id { get; set; }

        /**
         * Owner Profile
         */
        public int ProfileId { get; set; }
        [JsonIgnore]
        public virtual Profile Profile { get; set; }

        public string Name { get; set; }
        public string Category { get; set; }

        /**
         * Goals
         */
        public virtual List<Goal> Goals { get; set; }

        /**
         * Workouts
         */
        public virtual List<Join_ProgrammeWorkout> Workouts { get; set; }
    }
}