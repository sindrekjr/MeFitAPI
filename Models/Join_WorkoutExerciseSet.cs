namespace MeFit.Models
{
    public class Join_WorkoutExerciseSet
    {
        /**
         * Workout
         */
        public int WorkoutId { get; set; }
        public virtual Workout Workout { get; set; }

        /**
         * ExerciseSet
         */
        public int ExerciseSetId { get; set; }
        public virtual ExerciseSet ExerciseSet { get; set; }
    }
}