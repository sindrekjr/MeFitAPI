using System.Text.Json.Serialization;

namespace MeFit.Models
{
    public class ExerciseSet
    {
        /** Id **/
        public int Id { get; set; }

        /**
         * Owner Profile
         */
        public int ProfileId { get; set; }
        [JsonIgnore]
        public virtual Profile Profile { get; set; }

        /**
         * Exercise Info
         */
        public int ExerciseId { get; set; }
        public virtual Exercise Exercise { get; set; }
        public int Repetitions { get; set; }
    }
}