using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace MeFit.Models
{
    public class Workout
    {
        /** Id **/
        public int Id { get; set; }

        /**
         * Owner Profile
         */
        public int ProfileId { get; set; }
        [JsonIgnore]
        public virtual Profile Profile { get; set; }

        public string Name { get; set; }
        public virtual WorkoutType Type { get; set; }
        public int Complete { get; set; }

        /**
         * ExerciseSets
         */
        public virtual List<Join_WorkoutExerciseSet> ExerciseSets { get; set; }
    }

    public enum WorkoutType { }
}