namespace MeFit.Models
{
    public class Exercise
    {
        /** Id **/
        public int Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public virtual MuscleGroup MuscleGroup { get; set; }

        /**
         * Media references
         */
        public string Image { get; set; }
        public string Video { get; set; }
    }

    public enum MuscleGroup
    {
        Abs,
        Back,
        Chest,
        Calves,
        Hamstrings,
        Shoulders,
        Forearms,
        Biceps,
        Triceps,
        Quadriceps,
        Trapezius,
    }
}