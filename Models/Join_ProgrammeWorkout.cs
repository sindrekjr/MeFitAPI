namespace MeFit.Models
{
    public class Join_ProgrammeWorkout
    {
        /**
         * Programme
         */
        public int ProgrammeId { get; set; }
        public virtual Programme Programme { get; set; }

        /**
         * Workout
         */
        public int WorkoutId { get; set; }
        public virtual Workout Workout { get; set; }
    }
}