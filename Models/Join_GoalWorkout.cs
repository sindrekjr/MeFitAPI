using System;

namespace MeFit.Models
{
    public class Join_GoalWorkout
    {
        /** Id **/
        public int Id { get; set; }
        public virtual DateTime EndDate { get; set; }

        /**
         * Goal
         */
        public int GoalId { get; set; }
        public virtual Goal Goal { get; set; }

        /**
         * Workout
         */
        public int WorkoutId { get; set; }
        public virtual Workout Workout { get; set; }
    }
}