export class Profile {
  id: number;
  userId: string;
  height: number;
  weight: number;
  address: Address;
  programmes: Programme[];
  workouts: Workout[];
}

interface Address {
  id: number;
  addressLine1: string;
  addressLine2: string;
  addressLine3: string;
  postalCode: number;
  city: string;
  country: string;
}

interface Programme {
  id: number;
}

interface Workout {
  id: number;
}
