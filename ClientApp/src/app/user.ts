import { Profile } from './profile';

export class User {
  id: number;
  firstName: string;
  lastName: string;
  name: string;
  username: string;
  profile: Profile;
}