import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  users: User[];
  selectedUser: User;

  userForm: FormGroup;
  profileForm: FormGroup;

  formHasChanged: boolean = false;

  constructor(private userService: UserService, private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.userService.getUsers().subscribe(response => this.users = response);
  }

  userSelected() {
    this.userForm = this.formBuilder.group({
      name: new FormControl(this.selectedUser.name),
      id: new FormControl(this.selectedUser.id),
      username: new FormControl(this.selectedUser.username),
      address: new FormControl(this.parseProfileAddress()),
      firstName: new FormControl(this.selectedUser.firstName),
      lastName: new FormControl(this.selectedUser.lastName),
    });
    this.profileForm = this.formBuilder.group({
      id: new FormControl(this.selectedUser.profile.id),
      height: new FormControl(this.selectedUser.profile.height),
      weight: new FormControl(this.selectedUser.profile.weight),
      address: new FormControl(this.parseProfileAddress())
    });
  
    this.onChanges();
  }

  onChanges(): void {
    this.formHasChanged = false;
    this.userForm.valueChanges.subscribe(() => this.formHasChanged = true);
    this.profileForm.valueChanges.subscribe(() => this.formHasChanged = true);
    /*this.form.valueChanges.subscribe(form => {
      for(let key in this.form.controls) {
        let changed = false;

        let formControl = this.form.controls[key];
        if(formControl.value != this.selectedUser[key]) changed = true;

        this.formHasChanged = changed;
      }
    });*/
  }

  parseProfileAddress(): string {
    let addrObj = this.selectedUser.profile.address;
    let address = '';

    for(let key in addrObj) {
      let part = addrObj[key];
      if(part) address += `${address ? ', ' : ''}${part}`;
    }
    
    return address;
  }

  onSubmit(): void {
    for(let key in this.selectedUser) {
      if(!Object.keys(this.userForm).includes(key)) continue;
      this.selectedUser[key] = this.userForm.value[key];
    }

    for(let key in this.selectedUser.profile) {
      if(!Object.keys(this.profileForm).includes(key)) continue;
      this.selectedUser.profile[key] = this.profileForm[key];
    }

    this.userService.updateUser(this.selectedUser).subscribe(res => {
      /*let index = this.users.findIndex(u => u.id == this.selectedUser.id);
      this.selectedUser = res;
      this.users[index] = res;*/
    });
  }

  reset(): void {
    throw new Error("Method not implemented.");
  }

}
