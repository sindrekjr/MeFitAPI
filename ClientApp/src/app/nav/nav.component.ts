import { Component, OnInit } from '@angular/core';
import { appTitle } from '../common/global-constants';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {
  title = appTitle;

  constructor() { }

  ngOnInit() {
  }
}
