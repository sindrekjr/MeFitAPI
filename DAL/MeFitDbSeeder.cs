using System;
using System.Linq;
using System.Collections.Generic;

using Microsoft.EntityFrameworkCore;

using MeFit.Models;

namespace MeFit.DAL
{
    public static class MeFitDbSeeder
    {
        public static void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(SeedUsers());
            modelBuilder.Entity<Address>().HasData(SeedAddresses());
            modelBuilder.Entity<Profile>().HasData(SeedProfiles());
            modelBuilder.Entity<Programme>().HasData(SeedProgrammes());
            modelBuilder.Entity<Goal>().HasData(SeedGoals());
            modelBuilder.Entity<Workout>().HasData(SeedWorkouts());
            modelBuilder.Entity<ExerciseSet>().HasData(SeedExerciseSets());
            modelBuilder.Entity<Exercise>().HasData(SeedExercises());
        }

        /**
         * Seed User
         */
        private static IEnumerable<User> SeedUsers()
        {
            return new List<User>
            {
                new User { Id = 1, FirstName = "Princess", LastName = "Carolyn", Username = "princess", Password = "notthiscastle", Role = Role.Admin },
                new User { Id = 2, FirstName = "Bojack", LastName = "Horseman", Username = "bojack", Password = "horse-stable-battery", Role = Role.User }
            };
        }


        static IEnumerable<Address> SeedAddresses()
        {
            return new List<Address>
            {
                new Address { Id = 1, PostalCode = "90895", City = "Los Angeles", Country = "USA" }
            };
        }
        static IEnumerable<Profile> SeedProfiles()
        {
            return new List<Profile>
            {
                new Profile 
                { 
                    Id = 1,
                    UserId = 1,
                    AddressId = 1,
                    Height = 40,
                    Weight = 40,
                    Disabilities = "",
                    MedicalConditions = ""
                },
                new Profile 
                { 
                    Id = 2,
                    UserId = 2,
                    AddressId = 1,
                    Height = 70,
                    Weight = 160,
                    Disabilities = "",
                    MedicalConditions = ""
                }
            };
        }
        static IEnumerable<Programme> SeedProgrammes()
        {
            return new List<Programme>
            {
                new Programme { Id = 1, ProfileId = 1 },
                new Programme { Id = 2, ProfileId = 2 }
            };
        }
        static IEnumerable<Goal> SeedGoals()
        {
            return new List<Goal>
            {
                new Goal { Id = 1, ProgrammeId = 1 },
                new Goal { Id = 2, ProgrammeId = 2 }
            };
        }
        static IEnumerable<Workout> SeedWorkouts()
        {
            return new List<Workout>
            {
                new Workout { Id = 1, ProfileId = 1 },
                new Workout { Id = 2, ProfileId = 2 }
            };
        }
        static IEnumerable<ExerciseSet> SeedExerciseSets()
        {
            return new List<ExerciseSet>
            {
                new ExerciseSet { Id = 1, ExerciseId = 1, ProfileId = 1 },
                new ExerciseSet { Id = 2, ExerciseId = 1, ProfileId = 2 }
            };
        }
        static IEnumerable<Exercise> SeedExercises()
        {
            return Enum.GetValues(typeof(MuscleGroup)).Cast<MuscleGroup>().Select(m => new Exercise
            {
                Id = (int) m + 1,
                MuscleGroup = m
            });
        }
    }
}