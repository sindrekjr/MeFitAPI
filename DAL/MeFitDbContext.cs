using Microsoft.EntityFrameworkCore;

using MeFit.Models;

namespace MeFit.DAL
{
    public class MeFitDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Profile> Profiles { get; set; }
        public DbSet<Programme> Programmes { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<ExerciseSet> ExerciseSets { get; set; }
        public DbSet<Exercise> Exercises { get; set; }

        public DbSet<Join_GoalWorkout> JoinGoalWorkouts { get; set; }
        public DbSet<Join_ProgrammeWorkout> JoinProgrammeWorkouts { get; set; }
        public DbSet<Join_WorkoutExerciseSet> JoinWorkoutExerciseSets { get; set; }

        public MeFitDbContext() : base() { }

        public MeFitDbContext(DbContextOptions<MeFitDbContext> options) : base(options) { }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => optionsBuilder.UseLazyLoadingProxies();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /**
             * Setup joining table relations
             */
            modelBuilder.Entity<Join_GoalWorkout>().HasKey(gw => new { gw.GoalId, gw.WorkoutId });
            modelBuilder.Entity<Join_ProgrammeWorkout>().HasKey(pw => new { pw.ProgrammeId, pw.WorkoutId });
            modelBuilder.Entity<Join_WorkoutExerciseSet>().HasKey(ws => new { ws.WorkoutId, ws.ExerciseSetId });

            /**
             * Custom action specifications
             */
            modelBuilder.Entity<Workout>().HasOne(w => w.Profile).WithMany(x => x.Workouts).Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            /**
             * Seed data
             */
            MeFitDbSeeder.Seed(modelBuilder);
        }
    }
}